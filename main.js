var aero = {}
window.addEventListener('load', function () {
     aero = {
        loading: false,
        box:[],
        table:[],
        tableBody:[],
        tableStatus: [],
        data: [],
        timerRefreshData: [],
        init: function () { // инициализируем приложение
            aero.box = document.getElementById('aero');
            aero.table = document.querySelector('#aero-table');
            aero.tableBody = document.querySelector('#aero-table').querySelector('#aero-table-body');
            aero.tableStatus = document.querySelector('#aero').querySelector('#aero-status');

            // перестраховка на наличие контейниров
            if(aero.box && aero.table && aero.tableBody && aero.tableStatus){
                aero.getData();
                // запускаем таймер на обновление данных
                clearInterval(aero.timerRefreshData);
                aero.timerRefreshData = setInterval(function(){
                    aero.getData();
                }, 4000);
            }
            console.log('### aeroDATA - INIT');
        },
        returnRow: function(data){ // сортируем полученные данные и формируем результирующий HTML
            let cSortedDATA = data.sort(function(a,b){
                return a['distance'] - b['distance'];
            });
            let cResultHTML = '';
            for (let i=0; i<cSortedDATA.length; i++){
                // cResultHTML += `${aero.returnRowHTML(cSortedDATA[i], false)}`;
                // IE 11 - not supported `${template}`
                cResultHTML += aero.returnRowHTML(cSortedDATA[i], false);
            }
            return cResultHTML;
        },
        returnRowHTML: function(data, isNull){ //  формирем HTML строки таблицы - по умолчанию пустаая строка
            let cResult = '<tr><td></td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td><td>-</td></tr>';
            if(!isNull){
                cResult = '<tr><td></td>'
                +'<td>'+ data['number'] + '</td>'
                +'<td>'+data['regData'][0]+'/'+data['regData'][1]+'</td>'
                +'<td>'+data['coord'][0]+','+ data['coord'][0]+'</td>'
                +'<td>'+data['speed']+'</td>'
                +'<td>'+data['grad']+'</td>'
                +'<td>'+data['vert']+'</td>'
                +'<td>'+data['codeStart']+'</td>'
                +'<td>'+data['codeFinish']+'</td>'
                +'<td>'+data['distance']+'</td>'
                +'</tr>'
            }
            return cResult;
        },
        returnFormatDataRow: function(data){ // преобразование нужных нам данных в более удобный формат
            if(data[11] && data[12] && (Number(data[5])>0 && Number(data[4])>0)){
                let cData = {
                    number: data[9],
                    regData: [data[13],data[16]],
                    coord: [data[1],data[2]],
                    speed: data[5],
                    grad: data[3],
                    vert: data[4],
                    codeStart: data[11],
                    codeFinish: data[12],
                    distance: aero.getDistance(data[1], data[2]).toFixed(4) // обрезаем до 4-х символов после ","
                }
                return cData;
            } else {
                return false;
            }
        },
        returnHTML: function(data){ // разбираем полученные данные, преобразовываем в более читаемы объект. Вставляем HTML в таблицу
            let cResult = '';
            let cArray = [];
            let curData = data;
            if(typeof data ==='string' ){ curData = JSON.parse(data);}

            Object.getOwnPropertyNames(curData).forEach(function(val, idx, array) {
                if(curData[val][11] && curData[val][12] && (Number(curData[val][5])>0 && Number(curData[val][4])>0)){
                    /* если есть данные (т.е. самолёт находится в полёте):
                    * - код аэропорта вылета: data[val][11]
                    * - код аэропорта прибытия: data[val][12]
                    * - скорость больше 0: data[val][5]
                    * - высота над уровнем моря больше 0: data[val][4]
                     */
                    cArray.push(aero.returnFormatDataRow(curData[val]));
                }
            });
            cResult = aero.returnRow(cArray);
            // печатаем таблицу
            aero.tableBody.innerHTML = cResult;
        },
        getDistance: function(lat, lon){ // вычисляем дистанцию между аэропортом и самолётом
            let R = 6371; // радиус земли
            let aeroPort = [55.410307, 37.902451];
            let d=0;
            if(lat && lon && aeroPort[0] && aeroPort[1]){
                let dLatAero = aero.getRad(aeroPort[0]);
                let dLat = aero.getRad(lat);
                let dLatDiff = aero.getRad(aeroPort[0] - lat);
                let dLonDiff = aero.getRad(aeroPort[1] - lon);
                let a =
                    Math.sin(dLatDiff/2) * Math.sin(dLatDiff/2) +
                    Math.cos(dLatAero) * Math.cos(dLat) *
                    Math.sin(dLonDiff/2) * Math.sin(dLonDiff/2)
                ;
                let c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
                d = R * c; // Distance in km
            }
            return Number(d);
        },
        getRad: function(deg) { // радиус
            return deg * (Math.PI/180)
        },
        getData: function(){ // Запрашиваем данные с сервера, отображаем статус запроса и печатаем результат в таблицу
            aero.tableStatus.innerHTML = 'Loading...';
            // let curResult = ["Data NOT FOUND"];
            let cUrl = 'https://data-live.flightradar24.com/zones/fcgi/feed.js?bounds=56.84,55.27,33.48,41.48';
            let cBody = "{}";
            // 1. Создаём новый объект XMLHttpRequest
            let xhr = new XMLHttpRequest();

            // 2. Конфигурируем его: GET-запрос на URL 'phones.json'
            xhr.open('GET', cUrl, true);
            // xhr.setRequestHeader('Content-Type', 'application/json');
            xhr.responseType = "json";
            // xhr.withCredentials = true;

            xhr.onloadend = function(){
                if(xhr.status === 200){
                    let curResult = xhr.response;
                    aero.returnHTML(curResult);
                    aero.tableStatus.innerHTML = 'Loaded #';
                } else{
                    aero.tableBody.innerHTML = aero.returnRowHTML({}, true);
                    aero.tableStatus.innerHTML = 'Error #';
                }
            }

            // 3. Отсылаем запрос
            xhr.send(cBody);
        }
    }

    if(
        document.getElementById('aero')
        && document.querySelector('#aero-table')
        && document.querySelector('#aero-table').querySelector('#aero-table-body')
        && document.querySelector('#aero').querySelector('#aero-status')
    ){ aero.init();}
})